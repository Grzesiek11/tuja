use std::{mem, collections::VecDeque};

pub struct Decoder {
    buffer: Vec<u8>,
    queue: VecDeque<Input>,
    state: State,
}

impl Decoder {
    pub fn new() -> Self {
        Self {
            buffer: Vec::new(),
            queue: VecDeque::new(),
            state: State::Default,
        }
    }

    pub fn decode(&mut self, bytes: &[u8], bytes_read: usize) {
        for byte in bytes {
            self.state = match self.state {
                State::Default => if bytes_read > 1 && *byte == 0x1b {
                    State::Escape
                } else if *byte <= 127 {
                    self.queue.push_back(Input::from_char(*byte as char, false));
                    State::Default
                } else {
                    let mut prefix = 2;
                    let mut remaining_bytes = None;
                    for i in 0..3 {
                        prefix = prefix * 2 + 2;
                        if byte >> 5 - i == prefix {
                            self.buffer.push(*byte);
                            remaining_bytes = Some(i + 1);
                            break;
                        }
                    }
                    State::Utf8(remaining_bytes.unwrap(), false)
                },
                State::Utf8(remaining_bytes, alt) => {
                    self.buffer.push(*byte);
                    if remaining_bytes - 1 == 0 {
                        self.queue.push_back(Input::from_char(String::from_utf8(mem::replace(&mut self.buffer, Vec::new())).unwrap().chars().nth(0).unwrap(), alt));
                        State::Default
                    } else {
                        State::Utf8(remaining_bytes - 1, alt)
                    }
                },
                State::Escape => {
                    if bytes_read >= 2 && *byte > 127 {
                        // UTF-8 character with alt
                        // TODO: Duplicated code
                        let mut prefix = 2;
                        let mut remaining_bytes = None;
                        for i in 0..3 {
                            prefix = prefix * 2 + 2;
                            if byte >> 5 - i == prefix {
                                self.buffer.push(*byte);
                                remaining_bytes = Some(i + 1);
                                break;
                            }
                        }
                        State::Utf8(remaining_bytes.unwrap(), true)
                    } else if bytes_read == 2 && *byte <= 127 {
                        // ASCII character with alt or a escape (0x1b) character
                        self.queue.push_back(Input::from_char(*byte as char, *byte != 0x1b));
                        State::Default
                    } else {
                        // Sequence
                        State::Sequence(*byte, None)
                    }
                },
                State::Sequence(start_byte, keycode) => {
                    if (*byte as char).is_digit(10) {
                        self.buffer.push(*byte);
                        State::Sequence(start_byte, keycode)
                    } else {
                        let (keycode, modifier): (u8, u8) = if let Some(keycode) = keycode {
                            (keycode, String::from_utf8(mem::replace(&mut self.buffer, Vec::new())).unwrap().parse().unwrap())
                        } else {
                            (String::from_utf8(mem::replace(&mut self.buffer, Vec::new())).unwrap().parse().unwrap_or(1), 1)
                        };

                        let mut state = State::Default;
                        let key = match start_byte {
                            b'[' => match byte {
                                b'A' => Some(Key::Up),
                                b'B' => Some(Key::Down),
                                b'C' => Some(Key::Right),
                                b'D' => Some(Key::Left),
                                b'F' => Some(Key::End),
                                b'H' => Some(Key::Home),
                                b'~' => match keycode {
                                    2 => Some(Key::Insert),
                                    3 => Some(Key::Delete),
                                    5 => Some(Key::PageUp),
                                    6 => Some(Key::PageDown),
                                    15 => Some(Key::F5),
                                    17 => Some(Key::F6),
                                    18 => Some(Key::F7),
                                    19 => Some(Key::F8),
                                    20 => Some(Key::F9),
                                    21 => Some(Key::F10),
                                    23 => Some(Key::F11),
                                    24 => Some(Key::F12),
                                    _ => None,
                                },
                                b';' => {
                                    state = State::Sequence(start_byte, Some(keycode));
                                    None
                                },
                                _ => None,
                            },
                            b'O' => match byte {
                                b'P' => Some(Key::F1),
                                b'Q' => Some(Key::F2),
                                b'R' => Some(Key::F3),
                                b'S' => Some(Key::F4),
                                _ => None,
                            },
                            _ => None,
                        };

                        if let Some(key) = key {
                            let modifier = modifier - 1;
                            self.queue.push_back(Input {
                                inner: InputKind::Key(key),
                                shift: modifier & 0b1 != 0,
                                alt: modifier & 0b10 != 0,
                                control: modifier & 0b100 != 0,
                                meta: modifier & 0b1000 != 0,
                            });
                        }

                        state
                    }
                },
            };
        }
    }

    pub fn get_input(&mut self) -> Option<Input> {
        self.queue.pop_front()
    }
}

enum State {
    Default,
    Utf8(u8, bool),
    Escape,
    Sequence(u8, Option<u8>),
}

#[derive(Debug)]
pub struct Input {
    pub inner: InputKind,
    pub shift: bool,
    pub alt: bool,
    pub control: bool,
    pub meta: bool,
}

impl Input {
    fn from_char(char: char, alt: bool) -> Self {
        Self {
            inner: InputKind::Char(char),
            shift: false,
            alt,
            control: false,
            meta: false,
        }
    }
}

#[derive(Debug)]
pub enum InputKind {
    Char(char),
    Key(Key),
}

#[derive(Debug)]
pub enum Key {
    Insert,
    Delete,
    Home,
    End,
    PageUp,
    PageDown,
    Up,
    Down,
    Left,
    Right,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
}
