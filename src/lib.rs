use std::io::{self, Write};

use unicode_width::UnicodeWidthChar;

pub mod input;

#[cfg(windows)]
use windows::Win32::System::Console::{SetConsoleMode, GetStdHandle, STD_OUTPUT_HANDLE, STD_INPUT_HANDLE, ENABLE_PROCESSED_OUTPUT, ENABLE_VIRTUAL_TERMINAL_PROCESSING, ENABLE_VIRTUAL_TERMINAL_INPUT, GetConsoleScreenBufferInfo, CONSOLE_SCREEN_BUFFER_INFO};
#[cfg(unix)]
use {
    std::{fs::File, os::unix::prelude::AsRawFd, mem::MaybeUninit},
    libc::{termios, VMIN, tcgetattr, tcsetattr, ioctl, TIOCGWINSZ, winsize}
};

pub struct Terminal {
    pub panel: Panel,
    #[cfg(unix)]
    default_terminal_settings: termios,
}

impl Terminal {
    pub fn init() -> Self {
        #[cfg(windows)]
        let size = unsafe {
            let stdin_handle = GetStdHandle(STD_INPUT_HANDLE).unwrap();
            let stdout_handle = GetStdHandle(STD_OUTPUT_HANDLE).unwrap();

            SetConsoleMode(stdin_handle, ENABLE_VIRTUAL_TERMINAL_INPUT);
            SetConsoleMode(stdout_handle, ENABLE_PROCESSED_OUTPUT | ENABLE_VIRTUAL_TERMINAL_PROCESSING);

            let mut console_info = CONSOLE_SCREEN_BUFFER_INFO::default();
            GetConsoleScreenBufferInfo(stdout_handle, &mut console_info);

            ((console_info.srWindow.Right - console_info.srWindow.Left + 1) as usize, (console_info.srWindow.Bottom - console_info.srWindow.Top + 1) as usize)
        };
        #[cfg(unix)]
        let (default_terminal_settings, size) = {
            let mut default_terminal_settings = MaybeUninit::zeroed();
            let mut terminal_size: MaybeUninit<winsize> = MaybeUninit::zeroed();

            let stdin_descriptor = io::stdin().as_raw_fd();
            unsafe {
                tcgetattr(stdin_descriptor, default_terminal_settings.as_mut_ptr());
                let default_terminal_settings = default_terminal_settings.assume_init();

                let mut terminal_settings = default_terminal_settings.clone();
                terminal_settings.c_iflag = 0;
                terminal_settings.c_oflag = 0;
                terminal_settings.c_cflag = 0;
                terminal_settings.c_lflag = 0;
                terminal_settings.c_cc[VMIN] = 1;
                tcsetattr(stdin_descriptor, 0, &terminal_settings);

                ioctl(stdin_descriptor, TIOCGWINSZ, terminal_size.as_mut_ptr());
                let terminal_size = terminal_size.assume_init();

                (default_terminal_settings, (terminal_size.ws_col as usize, terminal_size.ws_row as usize))
            }
        };

        print!("\x1b[3J");
        io::stdout().flush().unwrap();

        Self {
            panel: Panel::new(size, (0, 0)),
            #[cfg(unix)]
            default_terminal_settings,
        }
    }

    pub fn end(&self) {
        #[cfg(unix)]
        {
            let stdin_descriptor = io::stdin().as_raw_fd();
            unsafe {
                tcsetattr(stdin_descriptor, 0, &self.default_terminal_settings);
            }
        }
    }

    pub fn update(&self) {
        print!("\x1b[2J");
        for (y, row) in self.panel.matrix.iter().enumerate() {
            for (x, cell) in row.iter().enumerate() {
                if let Some(cell) = cell {
                    print!("\x1b[{};{}H", y + 1, x + 1);
                    if cell.formatting.bold {
                        print!("\x1b[1m");
                    }
                    if cell.formatting.italic {
                        print!("\x1b[3m");
                    }
                    if cell.formatting.underline {
                        print!("\x1b[4m");
                    }
                    if cell.formatting.reverse {
                        print!("\x1b[7m");
                    }
                    match &cell.formatting.foreground_color {
                        Color::Color16(color16) => match color16 {
                            Color16::Black => print!("\x1b[30m"),
                            Color16::Red => print!("\x1b[31m"),
                            Color16::Green => print!("\x1b[32m"),
                            Color16::Yellow => print!("\x1b[33m"),
                            Color16::Blue => print!("\x1b[34m"),
                            Color16::Magenta => print!("\x1b[35m"),
                            Color16::Cyan => print!("\x1b[36m"),
                            Color16::White => print!("\x1b[37m"),
                            Color16::BrightBlack => print!("\x1b[90m"),
                            Color16::BrightRed => print!("\x1b[91m"),
                            Color16::BrightGreen => print!("\x1b[92m"),
                            Color16::BrightYellow => print!("\x1b[93m"),
                            Color16::BrightBlue => print!("\x1b[94m"),
                            Color16::BrightMagenta => print!("\x1b[95m"),
                            Color16::BrightCyan => print!("\x1b[96m"),
                            Color16::BrightWhite => print!("\x1b[97m"),
                        },
                        _ => (),
                    }
                    match &cell.formatting.background_color {
                        Color::Color16(color16) => match color16 {
                            Color16::Black => print!("\x1b[40m"),
                            Color16::Red => print!("\x1b[41m"),
                            Color16::Green => print!("\x1b[42m"),
                            Color16::Yellow => print!("\x1b[43m"),
                            Color16::Blue => print!("\x1b[44m"),
                            Color16::Magenta => print!("\x1b[45m"),
                            Color16::Cyan => print!("\x1b[46m"),
                            Color16::White => print!("\x1b[47m"),
                            Color16::BrightBlack => print!("\x1b[100m"),
                            Color16::BrightRed => print!("\x1b[101m"),
                            Color16::BrightGreen => print!("\x1b[102m"),
                            Color16::BrightYellow => print!("\x1b[103m"),
                            Color16::BrightBlue => print!("\x1b[104m"),
                            Color16::BrightMagenta => print!("\x1b[105m"),
                            Color16::BrightCyan => print!("\x1b[106m"),
                            Color16::BrightWhite => print!("\x1b[107m"),
                        },
                        _ => (),
                    }
                    print!("{}\x1b[0m", cell.char);
                }
            }
        }
        io::stdout().flush().unwrap();
    }

    pub fn draw_panel(&mut self, panel: &Panel) {
        for (y, row) in panel.matrix.iter().enumerate() {
            for (x, cell) in row.iter().enumerate() {
                *self.panel.get_cell_mut((panel.position.0 + x, panel.position.1 + y)) = cell.clone();
            }
        }
    }
}

pub struct Panel {
    size: (usize, usize),
    position: (usize, usize),
    matrix: Vec<Vec<Option<Cell>>>,
    cursor_position: (usize, usize),
    formatting: Formatting,
}

impl Panel {
    pub fn new(size: (usize, usize), position: (usize, usize)) -> Self {
        let formatting = Formatting::default();
        Self {
            size,
            position,
            matrix: vec![vec![None; size.0]; size.1],
            cursor_position: (0, 0),
            formatting,
        }
    }

    pub fn print_char(&mut self, char: char) {
        match char {
            '\n' => self.cursor_position = (0, self.cursor_position.1 + 1),
            _ => {
                let w = UnicodeWidthChar::width(char).unwrap();
                if self.cursor_position.0 + w > self.size.0 {
                    self.cursor_position = (0, self.cursor_position.1 + 1);
                }
                *self.get_cell_mut(self.cursor_position) = Some(Cell { char, formatting: self.formatting.clone() });
                self.cursor_position.0 += w;
            },
        }
    }

    pub fn print_str(&mut self, string: &str) {
        for char in string.chars() {
            self.print_char(char);
        }
    }

    pub fn set_cursor(&mut self, position: (usize, usize)) {
        self.cursor_position = position;
    }

    pub fn get_cursor(&self) -> (usize, usize) {
        self.cursor_position
    }

    pub fn get_size(&self) -> (usize, usize) {
        self.size
    }

    pub fn set_formatting(&mut self, formatting: Formatting) {
        self.formatting = formatting;
    }

    pub fn clear(&mut self) {
        for row in &mut self.matrix {
            for cell in row {
                *cell = None;
            }
        }
        self.set_cursor((0, 0));
    }

    fn _get_cell(&self, position: (usize, usize)) -> &Option<Cell> {
        &self.matrix[position.1][position.0]
    }

    fn get_cell_mut(&mut self, position: (usize, usize)) -> &mut Option<Cell> {
        &mut self.matrix[position.1][position.0]
    }
}

#[derive(Clone)]
struct Cell {
    char: char,
    formatting: Formatting,
}

#[derive(Clone)]
pub struct Formatting {
    pub bold: bool,
    pub italic: bool,
    pub underline: bool,
    pub reverse: bool,
    pub foreground_color: Color,
    pub background_color: Color,
}

impl Formatting {
    pub fn default() -> Self {
        Self {
            bold: false,
            italic: false,
            underline: false,
            reverse: false,
            foreground_color: Color::Default,
            background_color: Color::Default,
        }
    }
}

#[derive(Clone)]
pub enum Color {
    Default,
    Color16(Color16),
    Color256(u8),
    TrueColor(u8, u8, u8),
}

#[derive(Clone)]
pub enum Color16 {
    Black,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White,
    BrightBlack,
    BrightRed,
    BrightGreen,
    BrightYellow,
    BrightBlue,
    BrightMagenta,
    BrightCyan,
    BrightWhite,
}
